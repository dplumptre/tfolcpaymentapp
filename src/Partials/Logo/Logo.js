import React from "react";
import lg from '../../assets/images/logo.png';

const logo = (props) => (
<img className={props.classes} src={lg} alt="The fountain of Life Church"/>
);

export default logo;