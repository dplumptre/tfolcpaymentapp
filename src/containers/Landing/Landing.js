import React, { Component } from 'react';
import Accounts from "../../components/accounts/accounts";
import Aux from '../../HOC/Aux';

class Landing extends Component {

    state={
        showAccounts: false
    }

    showAccountsHandler = () =>{
        this.setState({showAccounts:!this.state.showAccounts});
    }


    ForeignPaymentHandler =() =>{
        this.props.history.push('/foreign-payment');
    }

    LocalPaymentHandler =() =>{
        this.props.history.push('/local-payment');
    }

    render(){

        let acc = null;
        if(this.state.showAccounts){
            acc=(<Accounts/>);
        }


    

    return(

        <Aux>
                     
            <div className="row">
            <div className="col-sm-12 mb-3">
            <button type="button" onClick={this.showAccountsHandler} className="btn btn-danger btn-lg btn-block">
            BANK TRANSFER (OFFLINE)
            </button>
            </div>
            { acc}
            </div>
            
            <div className="row">
            <div className="col-sm-12 mb-3">
            <button type="button" onClick={this.ForeignPaymentHandler} className="btn btn-primary btn-lg btn-block">
            DOLLAR 
            </button>
            </div>
            </div>    
                
            <div className="row">
            <div className="col-sm-12 mb-3">
            <button type="button"  onClick={this.LocalPaymentHandler} className="btn btn-success btn-lg btn-block">
            NAIRA 
            </button>
            </div>
            </div>              
            
            </Aux>   
        )
    }
}

export default Landing;